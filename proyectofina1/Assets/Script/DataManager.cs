﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class DataManager : MonoBehaviour
{
    public string playerFileName;
    public static DataManager instance;

    //private HUBManager scores;
    private Score score;
    private StreamReader sr;
    private StreamWriter sw;


    void Awake()
    {
        instance = this;
        score = new Score();
    }
    

    public Score GetData()
    {
#if FILE_SYSTEM
        if (File.Exists(Application.persistentDataPath+"/"+playerFileName))
        {
            sr = new StreamReader(Application.persistentDataPath + "/" + playerFileName);
            score=JsonUtility.FromJson<Score>(sr.ReadToEnd());
            sr.Close();
        }

#else 
        if (PlayerPrefs.HasKey("Score"))
        {
            score.score = PlayerPrefs.GetInt("Score");
            score.bestscore = PlayerPrefs.GetInt("Bestscore");
           
        }
#endif
        else
        {
            score.scored = 0;
            score.bestscore = 0;
            
        }


        return score;
    }

    public void SaveData(Score p)
    {
#if FILE_SYSTEM
        sw = new StreamWriter(Application.persistentDataPath + "/" + playerFileName, false);
        sw.Write(JsonUtility.ToJson(p));
        sw.Close();


#else
        PlayerPrefs.SetInt("Score", p.score);
        PlayerPrefs.SetInt("BestScore", p.score);
   
        PlayerPrefs.Save();
#endif

    }
}
