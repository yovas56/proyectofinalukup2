﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public enum EnemyState
{
    PATROL, CHASE
};

public class EnemyController : MonoBehaviour
{
    public Vector2 patrolRange;
    public float updatePatrolPosition;
    public float distanceToChase;

    private NavMeshAgent enemyAgent;
    private Vector3 target;
    private Transform player;
    private Animator enemyAnimator;
    private EnemyState currentSate;

    private RaycastHit visionHit;
    private bool valenemy = false;
    private bool aux = true;

    // Start is called before the first frame update
    void Start()
    {
        currentSate = EnemyState.PATROL;
        enemyAgent = GetComponent<NavMeshAgent>();
        enemyAnimator = GetComponent<Animator>();
        player = GameObject.FindGameObjectWithTag("Player").transform;
        InvokeRepeating("CalculateDestination", 0, updatePatrolPosition);

    }

    // Update is called once per frame
    void Update()
    {
        /*  if ((transform.position - player.position).magnitude <= distanceToChase)
          {

          }*/


        if (Physics.SphereCast(transform.position, 3, transform.forward, out visionHit) && BarlifeEnemy.instance.Empty()==true)
        {
            //Debug.Log("Vision Hit: " + visionHit.transform.name);
            if (visionHit.transform.tag == "Player")
            {
                DetectPlayer();
            }
        }
        enemyAnimator.SetFloat("speed", enemyAgent.velocity.magnitude);
        if (currentSate == EnemyState.CHASE && BarlifeEnemy.instance.Empty()==true)
        {
            target = player.position;
        }
        enemyAgent.SetDestination(target);

        if (BarlifeEnemy.instance.Empty() == false && aux==true)
        {   
            enemyAnimator.SetTrigger("Dead");
            
            Move.instance.Score();
            valenemy = true;
            aux = false;
        }

        if (valenemy)
        {
            Move.instance.Score();


            valenemy = false;
        }

        



    }

    void CalculateDestination()
    {
        target = transform.position +
        new Vector3(Random.Range(patrolRange.x, patrolRange.y), 0, 0);


    }
    void OnTriggerEnter(Collider collider)
    {
        enemyAnimator.SetTrigger("Figth");

        if (collider.transform.tag == "Player")
        {
            Debug.Log("Colision con personaje");
            

            DetectPlayer();
        }

        
    }



   

    void DetectPlayer()
    {
        currentSate = EnemyState.CHASE;
        CancelInvoke("CalculateDestination");
    }
}
