﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class TimeGameOver : MonoBehaviour
{
    public int tiempoentero;
    public float tiempototal;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        tiempototal -= Time.deltaTime;
        tiempoentero = (int)tiempototal;

        if (tiempoentero <= 0)
        {
            SceneManager.LoadScene(0, LoadSceneMode.Single);
        }
    }
}
