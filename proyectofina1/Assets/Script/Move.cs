﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Move : MonoBehaviour
{
    private floor piso;
    private Rigidbody movimiento;
    Vector3 dir;
    Vector3 controldir;
    Vector3 dirsalto;
    public float velocidad;
    public float salto;
    public float gravedad;
    public Transform mano;
    private float tiempoescena;
    private int tiempotran;
    private int score = 0;
    private int bestscore = 0;
    private bool inEnemy;
    public Score dscore;


    //public VirtualJostick virtualjostick;
    public static Move instance;
    private bool Jumpbool=false;
    private bool Hitbool = false;
    private static int numintentos;

    void Awake()
    {
        dscore = new Score();
        instance = this;
        movimiento = GetComponent<Rigidbody>();
        piso = GetComponent<floor>();
        //virtualjostick = GetComponent<VirtualJostick>();
    }
    void Start()
    {
        dscore = DataManager.instance.GetData();
        
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Baragua.instance.Empty() && BarLife.instance.Empty()==true)
        {
            controldir = new Vector3(VirtualJostick.instance.Horizontal(), 0, 0);

        }

        else
        {
            controldir = new Vector3(0, 0, 0);

            tiempoescena += Time.deltaTime;
            Debug.Log("numero de intentos" + numintentos);
            tiempotran = (int)tiempoescena;
            if (tiempotran > 3 )
            {
                numintentos++;
                SceneManager.LoadScene(2, LoadSceneMode.Single);

            }
            if (numintentos >= 3)
            {
                numintentos = 0;
                SceneManager.LoadScene(4, LoadSceneMode.Single);
                
            }

        }


        if (piso.isGrounded || inEnemy==true)
        {
            inEnemy = false;
            dir.x = -controldir.x * velocidad;
            dir.y = -1;
            if (controldir.x != 0)
            {
                transform.rotation = Quaternion.LookRotation(-controldir);
            }

            if (Jumpbool && Baragua.instance.Empty() && BarLife.instance.Empty() == true)
            {

                dir.y = salto;
                piso.isGrounded = false;
            }

            
        }
        else
        {
            dirsalto = new Vector3(-VirtualJostick.instance.Horizontal(), 0, 0);
            dir.x += dirsalto.x*Time.deltaTime*5;
            dir.y -= gravedad * Time.deltaTime;
        }
        movimiento.velocity = dir;
        
        score = dscore.scored;
        BestScore();

    }

    public void JumpON()
    {
        Jumpbool = true;
        
        //return Input.GetButtonDown("Jump");
    }

    public void JumpOff()
    {
        Jumpbool = false;
    }

    public bool AnimeJump()
    {
        return Jumpbool;
    }

    public void aniHitOn()
    {
        Hitbool = true;
    }

    public void aniHitOff()
    {
        Hitbool = false;
    }


    public bool AnimeHit()
    {
        return Hitbool ;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Items")
        {
            other.transform.position = mano.position;
            other.transform.parent = mano.transform;
            HUBManager.instance.SetObjet("Escoba");
        
        }

        if (other.transform.tag == "Enemy")
        {
            inEnemy = true;
        }

    }

    private void OnTriggerStay(Collider other)
    {
        if (other.transform.tag == "Agua")
        {
            Baragua.instance.LowBar();
        }
    }

    public void SetScore(int setscore)
    {
        dscore.scored=setscore;
    }

    public int Score()
    {
        dscore.scored=10;
        HUBManager.instance.SetScore(dscore.scored);
        return score;
    }

    public int BestScore()
    {
        if (score > dscore.bestscore)
        {


            dscore.bestscore = score;
            HUBManager.instance.SetBestScore(score);
        }
        return bestscore;
    }
}

