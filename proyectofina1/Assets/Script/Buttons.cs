﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Buttons : MonoBehaviour
{
    public static Buttons instance;
    private bool isPaused;


    // Start is called before the first frame update
    void Awake()
    {
       
        instance = this;
        isPaused = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ButtonJumpON()
    {
        Move.instance.JumpON();
    }

    public void ButtonJumpOFF()
    {
        Move.instance.JumpOff();
    }

    public void ButtonFigthOn()
    {
        Move.instance.aniHitOn();
    }

    public void ButtonFigthOff()
    {
        Move.instance.aniHitOff();
    }

    public void Buttoninicio()
    {
        SceneManager.LoadScene(2,LoadSceneMode.Single);
    }

    public void ButtonEscena2()
    {
        SceneManager.LoadScene(1, LoadSceneMode.Single);
    }

    public void ButtonPerson()
    {
        SceneManager.LoadScene(3,LoadSceneMode.Single);
    }
    

    public void ButtonOption()
    {

    }

    public void ButtonExit()
    {
        //DataManager.instance.SaveData(Move.instance.dscore);
        Application.Quit();
    }

    public void PauseGameOn()
    {
        HUBManager.instance.ShowPuase();
        HUBManager.instance.PauseGame();    
    }

    public void PauseGameOff()
    {
        HUBManager.instance.ShowHUD();
        HUBManager.instance.PauseGame();
    }


}
