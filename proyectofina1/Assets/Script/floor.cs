﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class floor : MonoBehaviour
{
    [HideInInspector]
    public bool isGrounded;

    Collider Piso;

    float Wait;

    public void SetWait(float W)
    {
        Wait = W;

        isGrounded = false;
        Piso = null;
    }

    void Update()
    {
        Wait -= Time.deltaTime;

        if (Wait < 0)
            Wait = 0;
    }

    void OnCollisionEnter(Collision col)
    {
        if (Wait == 0 && col.collider.tag == "Piso")
        {
            if (Vector3.Angle(Vector3.up, col.contacts[0].normal) < 45)
            {
                isGrounded = true;

                Piso = col.collider;
            }
        }
    }

    void OnCollisionStay(Collision col)
    {
        if (Wait == 0 && (col.collider.tag == "Piso" || col.collider.tag == "Pared"))
        {
            if (Vector3.Angle(Vector3.up, col.contacts[0].normal) < 45)
            {
                isGrounded = true;

                Piso = col.collider;
            }
        }
    }

    void OnCollisionExit(Collision col)
    {
        if (col.collider == Piso)
        {
            isGrounded = false;

            Piso = null;
        }
    }

}
