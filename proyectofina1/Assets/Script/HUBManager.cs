﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HUBManager : MonoBehaviour
{
    public TextMeshProUGUI Object;
    public TextMeshProUGUI time;
    public TextMeshProUGUI Score;
    public TextMeshProUGUI BestScore;
    public GameObject HUDPanel;
    public GameObject PausePanel;
    public Slider musicSlider;
    public Slider sfxSlider;

    private bool isPaused;
    private bool paused;
  
    public int tiempoentero;
    public float tiempototal;
    public static HUBManager instance;
    
    public void Awake()
    {
        instance = this;
        isPaused = true;
        paused = false;
    
        //SetTime(valtiempo);
        //SetScore(0);
        //SetBestScore(10);
    }

    void Update()
    {
        tiempototal -= Time.deltaTime;
        tiempoentero = (int)tiempototal;

        SetTime(tiempoentero);
        
        
    }


    public void SetTime(int settime)
    {
        
        time.text = settime.ToString();
    }

    public void SetScore(int setscore)
    {
        Score.text = setscore.ToString();

    }

    public void SetBestScore(int setBestScore)
    {
        BestScore.text = setBestScore.ToString();
    }

    public void SetObjet(string NomObjet)
    {
        Object.text ="Objeto:" + NomObjet;
    }

    public void PauseGame()
    {
        if (isPaused)
        {
            Time.timeScale = 0f;
            paused = true;
        }
        else
        {
            Time.timeScale = 1f;
            paused = false;
        }
        isPaused = !isPaused;
    }

    void CleanPanels()
    {
        HUDPanel.SetActive(false);
        PausePanel.SetActive(false);
    }

    public void ShowHUD()
    {
        CleanPanels();
        HUDPanel.SetActive(true);
    }

    public void ShowPuase()
    {
        CleanPanels();
        PausePanel.SetActive(true);
    }

    public void SetMusicVolume(float volume)
    {
        musicSlider.value = volume;

    }

    public void GetMusicVolume(float SValue)
    {
        //AudioManager.instance.SetMusicVolume(SValue);
    }

    public void SetSFXVolume(float volume)
    {
        sfxSlider.value = volume;

    }

    public void GetSFXVolume(float SValue)
    {
        //AudioManager.instance.SetSFXVolume(SValue);
    }
}
