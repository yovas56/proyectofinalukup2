﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BestScore : MonoBehaviour
{
    public int score;
    public int bestscore;
    public static BestScore instance;

    void Awake()
    {
        instance = this;
        
    }

    void Update()
    {
        //score = Move.instance.Score();
        //HUBManager.instance.SetScore(score);
        if (score > bestscore)
        {
            HUBManager.instance.SetBestScore(score);
        }
    }
}
