﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    Animator Playanim;
    AnimatorStateInfo currentStateAnim;
    CapsuleCollider cap;
    public static PlayerController instance;
    // Start is called before the first frame update
    void Awake()
    {
        instance = this;
        Playanim = GetComponent<Animator>();
        cap = GetComponent<CapsuleCollider>();
    }

    // Update is called once per frame
    void Update()
    {
        currentStateAnim = Playanim.GetCurrentAnimatorStateInfo(0);
        Playanim.SetFloat("direccion", VirtualJostick.instance.Horizontal());
        Playanim.SetFloat("Crounch", VirtualJostick.instance.Vertical());

        if (Move.instance.AnimeJump())
        {
            Playanim.SetTrigger("Jump");
        }

        if (Move.instance.AnimeHit())
        {
            Playanim.SetTrigger("Hit");
        }
       
    }
}
