﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controlcamare : MonoBehaviour
{
    Vector3 aux;
    public int disz;
    public int disx;
    public int disy;
    private Transform target;
    
    // Start is called before the first frame update
    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").transform;

    }

    // Update is called once per frame
    void Update()
    {
        aux = transform.position;
        aux.x = target.position.x;
        aux.y = target.position.y;
       

        transform.position = aux;

        transform.LookAt(target.position);
        

    }
}
