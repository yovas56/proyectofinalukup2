﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Player : MonoBehaviour
{

    public int livebar;
    public int oxigbar;
    public float sfxVolumen;
    public float musicVolumen;

}
