﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarObjeto : MonoBehaviour
{
    public Image livebar;
    public Color FullColor;
    public Color EmptyColor;
    private float liveTmp = 1;
    public float resistance;
    public static BarObjeto instance;


    // Start is called before the first frame update
    void Awake()
    {
        instance = this;

        SetLiveLevel(1);
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.L))
        {
            liveTmp -= resistance;
            SetLiveLevel(liveTmp);
        }
    }

    public void SetLiveLevel(float liveLevel)
    {
        livebar.color = Color.Lerp(EmptyColor, FullColor, liveLevel);
        livebar.fillAmount = liveLevel;

    }

    public void LowBar()
    {
        liveTmp -= resistance;
        SetLiveLevel(liveTmp);

    }

    public bool Empty()
    {
        if (liveTmp < 0)
        {
            return false;
        }
        else
            return true;
    }

}