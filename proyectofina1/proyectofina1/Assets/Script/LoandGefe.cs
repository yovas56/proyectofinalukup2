﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoandGefe : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject Escenario1;
    public GameObject EscenarioGefe;
    private int entro;
    void Update()
    {
        
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Player")
        {
            entro++;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.transform.tag == "Player")
        {
            Escenario1.SetActive(false);
            EscenarioGefe.SetActive(true);
        }

    }

    
}
